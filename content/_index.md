+++
title = "Hi!"
+++

# I'm Alex McKenzie

I’m a data scientist looking to build the next generation of safe, reliable and trusted AI.

With a background in Maths and Philosophy, I’m equally comfortable coding, researching and writing. I’ve filled many different roles in my career, including NLP, data engineering, project management and QA automation. Currently I'm looking for work in AI Safety - hit me up!

When I’m not at work, I am often thinking about how to effectively improve the world with Effective Altruism or [at my piano](https://www.youtube.com/@alexmckalexmck).

Want to contact me? Email me at hello@alexmck.com.
