+++
title = "An Unfinished Glossary for Good AI"
date = 2021-01-03T23:00:00Z
draft = true
+++

As I become more and more seriously invested in "good AI" as a whole, I'm beginning to understand the distinctions and relationships between various sub-fields and communities. Here's my attempt to explain a host of related terms and how they interact with one another. Note: this is a living document, which I intend to keep updating as I learn more.

- _Good AI_ is a term I'm using as a blanket, catch-all for any work which attempts to make AI work well for humanity, either now or in the future
- _AI Safety_ refers to efforts to make AI systems safe, or in other words, to make the user (or humanity in general) safe from these AI systems.
- _AI Alignment_ refers to a sub-field of AI Safety which focuses on getting AI systems to do what humans want them to do. Mostly this is highly technical work. A summary of the latest AI Alignment research can be found at the AI Alignment newsletter.
- _AI Reliability_ is about building AI which does what we expect it to do, for both known and new situations. As far as I can tell, the difference between AI Reliability and AI Alignment is that the latter term is used only by a particular community, whereas the latter is more broad-umbrella.
- _AI Verification_ is the practice of formally verifying (i.e. producing a proof) particular properties of AI systems. It is touted as a way to achieve AI Reliability. In conversations I've had, some AI Alignment researchers do not think that AI Verification is the best approach for achieving alignment.
- _AI Ethics_ is a very broad term, covering all ways of making AI systems behave ethically or understand ethics.
- _AI Bias_ is a specific ethical problem with AI systems, concerning how they may be biased towards or against particular groups or traits.
- _Explainable AI_ refers to building AI systems which admit explanations, i.e. AI systems which let you ask the question "why did you make that decision?".
- _Interpretable AI_
- _Robust AI_
