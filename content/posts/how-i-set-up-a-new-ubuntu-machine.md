+++
title = "How I Set Up a New Ubuntu Machine"
date = 2020-06-20T18:00:00Z
draft = true
+++

This is mostly for my own benefit.

- Install zsh and oh-my-zsh
- Set up python:
  - Install pyenv and pyenv-virtualenv using pyenv-installer
  - Install python build dependencies (specified by pyenv)
  - Use pyenv to install the latest python and set it as default
  - Install miniconda (not inside pyenv)
  - Install pipx
  - Install poetry using pipx
- Install node
  - Install nvm first
  - Instructions: [https://docs.microsoft.com/en-us/windows/nodejs/setup-on-wsl2](https://docs.microsoft.com/en-us/windows/nodejs/setup-on-wsl2 "https://docs.microsoft.com/en-us/windows/nodejs/setup-on-wsl2")
- Install linuxbrew
- Install autojump
- Install hyper (nice terminal)
  - If on linux, preferences will be automatically configured upon installing dotfiles
  - If on windows, this should be done manually
- Install sdkman (for installing java or scala)
- Install ripgrep (better grep / find)
- Install miniconda (for data science)
- Install and configure dotfiles:
  - Clone dotfiles repo
  - Run install script
  - Install gitwatch
  - Add to startup programs
- Install vim.plug plugins: open vim and type `:PlugInstall`
- Create Projects and Software folders
- Install nice fonts:
  - \[Powerline fonts\](https://github.com/powerline/fonts), using \[this tutorial\](https://medium.com/@slmeng/how-to-install-powerline-fonts-in-windows-b2eedecace58) if on windows
  - \[JetBrains Mono for Powerline\](https://github.com/seanghay/JetBrainsMono-Powerline), which is just the best font

To do better:

- Set up dotfiles to use local env variables
- Add script that automates some of this work
