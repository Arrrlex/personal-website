+++
title = "Programming and Play"
date = "2019-11-23T11:41:11+01:00"
draft = false
+++

When I was around seven or eight, I loved to play a made-up game involving dragons and demons. There weren’t many rules, but it was great fun, and nobody ever got hurt because when I fired a gun at my friend he would fire back, and the bullets would clatter harmlessly to the ground somewhere in between.

I’ve always loved to “play” piano - not just to play it, but actually to engage playfully in experimentation. Sometimes I actively try to produce sounds that are jarring, perhaps by playing the same tune with both of my hands but shifting one hand by a semitone. (I apologize to my parents and brothers for doing that so often.) Doing this helped me understand harmony and rhythm, and to begin to feel out the parameters of my creativity and ability.

Today I spend many evenings writing and performing, but my day job is software: reading documentation, writing code, hitting “compile”. Where’s the playfulness in that? Does it even matter?

Developmental psychologists love to talk about “learning through play”. For them, an activity counts as play if its self-directed, fun, has no exterior purpose and contains some degree of make-believe. Play is believed to be (one of) the most important components of a child’s learning. It’s when the child does the work of synthesising and distilling all the information they’ve absorbed. Kids do this instinctively, and they love to do it. Perhaps the best thing grownups can do to encourage play is to provide rich, varied environments in which lots is possible and no goals are prescribed (like a lego set).

As we get older, the environments get bigger but the rules more restrictive. Being a grownup can feel like knowing there are vast opportunities for play, and choosing to forego them. But that’s a broader topic than this blog post; here I have the more modest goal of exploring play in programming.

Most programming languages (with the notable exception of perl) have very restrictive rules; the syntactically valid programs are a small fraction of the space of text files, likewise for semantically valid programs, likewise for interesting programs. But every form of play has rules: if you’re playing doctor you can’t feed the patient cyanide, if you’re playing dragons ‘n’ demons you can’t both shoot each other simultaneously, and in either case you can’t actually throw a punch or urinate or fart the alphabet backwards. The space of interesting and fun playtime sessions is a small subset of the space of possible behaviours. So simply having rules, even restrictive rules, doesn’t preclude programming from being a component of play. (Alternatively, you may view interacting with the rules via the compiler as part of the game you’re playing with the computer.)

Some programming practices lend themselves more readily to play than others. A REPL encourages developers to experiment, and since feedback is immediate goals can be transient or nonexistent: for example, “what happens if I do this?”-style goals. Some programming tutorials encourage using a REPL for learning through play, e.g. by asking the student to try and write code that produces errors or crashes in interesting ways. By encouraging students to play around with the rules of the language, the learner can understand the true boundaries of those rules.

Another way of playing, other than playing with the language itself, is using code to play with data. Using a jupyter notebook, one might experiment with different views into the data, or reshaping it, or visualising it creatively.

I think playing is important and rewarding, and I’d encourage you to put a playful mindset in your toolbox for learning and enjoyment.
