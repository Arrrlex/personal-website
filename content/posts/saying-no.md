+++
title = "Say No!"
date = "2020-01-12T12:28:42+01:00"
draft = false
+++

This is a short, obvious post about my ongoing experience with saying no in my early career. There's probably nothing new here, but it's worth saying nonetheless.

I entered software development with what I felt was entirely inadequate experience. I had a degree in Maths and Philosophy which involved precisely zero coding (unless you count Gödel numbering), a three-week unpaid internship which had dampened my confidence, and a small Django project which crashed when tested by my well-meaning friends.

Thus, when I was offered a job at a startup in Berlin, my first response was "yes!" - of course I accepted. When asked if I wanted to start work 2 days before my contract officially started, that same enthusiastic "yes!" was out of my lips before the CEO had finished his sentence. That was the first of many stupid times I said "yes!" during that job.

What did I learn from my experiences? That saying no is difficult, because:

1. I wanted to be seen as helpful and willing to get stuck in;
1. I wanted to learn as much as possible, especially to "catch up" with more experienced engineers;
1. So much stuff sounded interesting

To be fair, lots of things I ended up doing were fascinating, and I did in fact learn a lot. But there were two key problems with my approach.

Firstly, it stressed me out. I wasn't able to moderate how much I had on my plate at any one moment, and deadlines would constantly approach me from all angles.

Secondly, I was unable to be reliable. When asked to give an estimate for a project, I wouldn't take into account my workload. Estimation as a software engineer is a really, really difficult problem anyway, especially for a freshly minted graduate developer, but *especially* if your estimates are in any way "aspirational", which they had to be to justify taking on so many projects.

I also think that, in some unspoken way, I developed something of a reputation for having "cheap time". Since I was always enthusiastic about doing extra work, people had no problem with asking me to do things. Thus when I finally got into the habit of saying no more often, I began training my colleagues (and myself) to treat my time as a scarce resource. I think this actually made me more valued as a team member, overall.
