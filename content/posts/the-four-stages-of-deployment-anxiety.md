+++
title = "The Four Stages of Deployment Anxiety"
date = "2023-10-01T19:00:00+01:00"
draft = true
+++

I think it's fair to say there are certain beliefs that software engineers as a tribe hold dear, many of which concern deployment: taking a software artifact from its development environment and thrusting it into the Real World where it can confuse and enrage actual users. Here I'll describe the 4 stages of belief that software engineers pass through, on their way to true deployment enlightenment. Please enjoy.

## Stage 1: deployment is a holy ritual

Initially, there is nothing more revered and feared than deployment. I certainly remember feeling real terror as a junior developer seeing my code go out into the wild, knowing that it would be _my fault_ if something went wrong. My fault!

Since deployment is so feared, it makes sense that it is not a common occurrence, and when it does occur there should be much ceremony and ritual in order to _make sure nothing goes wrong_. One way that teams achieve this is via having "deploy days", e.g. the first Monday of every month, when all accumulated changes are deployed.

Knowing we will deploy at a particular time gives us predictability: for one week before the deploy, we can stop working on new features and instead focus on testing, bug squashing, and polish. Apart from this _one_ feature that's of course necessary in the next release.

When the deployment occurs, everyone gathers in hushed silence around the senior developer's computer as they hit the big red button. All their collective work over the last month leaves the realm of the theoretical and becomes real. This is the worst part: somehow, everyone knows in their gut that something will go wrong, because there are always problems in production, but no-one is sure where they'll come from.

As silently predicted, something went wrong, and the next few hours are panic: everyone is scrambling to discover what the problem is exactly. Someone solves it and the big red button is rapidly pushed again, only to discover this didn't fix the problem, it fixed some _other_ problem, and it didn't even fix that one properly. Sirens are blaring, several people are screaming in the next room, and you're pretty sure you can smell fire. Such is life on deploy day.

## Stage 2: deployment on request

One day, you make a small fix to a problem already in production (there are always problems in production). "Surely", you implore Senior Developer, "we don't need to wait until deploy day for this tiny one-liner?" Senior Developer remembers the chaos and destruction of the last deploy day, and agrees to let you make this one little change.

In order to enable more frequent deploys for small changes, we need a simple process for deploying. This process should not involve too many steps, but - importantly - it will include a manual review phase from a trusted party. The nice thing about this phase is it's a natural point at which to review how this change might interact with other changes, either already merged or in progress.

As you haven't yet made it into the Inner Circle, you need to ask one of them for approval. This can be scary - they are very tall, with dark shadows under their eyes bearing witness to the sleepless nights they endured after Deploy Days, earning them their coveted positions. They are sure to project their wariness of deployments onto you.

## Stage 3: click OK to deploy

As you progress through your career and you gradually gain the trust of your peers, you might wonder: what's the point of an ad-hoc deployment review process if no-one uses it?

You note that, whenever small changes have been deployed ad-hoc, any problems have been caught soon, and corrected usually before a tranche of obfuscating changes have had a chance to muddy the waters. And, as you remind yourself, there are always problems in production.

In a sweeping change, you remove the requirement for deployments to be "requested". Now, whenever a change is merged into the main codebase, a deployment is triggered - pending, of course, review from a member of the Inner Circle.

This way you can preserve the safety and security of Stage 2, while removing the need for bravery from change authors. Deploys become smaller and more frequent.

The main drawback of this approach is the extra demands it places on the Inner Circle's already tight schedule. But the masses love it, and changes get ever smaller and more frequent.

## Stage 4: YOLO deploys

(Also known as "Continuous Delivery" but I prefer my name.)

By this stage, your hair is long and matted, your face is lined, and madness dances around your eyes. You have tried everything to make deploying safe and reliable, and yet there are always problems in production. _There are always problems in production._

Don't worry. This is the path to enlightenment. You must genuinely believe that _there are always problems in production._ Once you truly understand what these words mean, you begin to see beyond them.

- We will never, ever catch all bugs before they go live. In that case, why wait? Deploy them now! Expose them to the light of day, so we can fix them sooner.
- Similarly, since we know that our code has bugs, we should make each change as small as possible - and deploy it as soon as possible - in order to give us the best chance of isolating the change that caused some problem.
- Since _there are always problems in production_, we need to focus on monitoring, so that we can catch those problems early.
- A problem might arise at any time of day or night - remember, since _there are always problems in production_, the question is not if, it's when they are caught by our monitoring systems. We don't want to be called for every problem that is (inevitably) discovered in production. Instead, the first recourse should be automatic: automatically rolling back to the last known safe version, only notifying humans if really necessary.

You feel yourself grow taller. You have reached the Inner Circle. What's more, you look around, and with astonishment, you realise that _everyone_ is in the Inner Circle.

A smile plays about your lips. You click "merge".
