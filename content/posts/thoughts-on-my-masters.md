+++
title = "Thoughts on my Masters"
date = "2022-11-04T17:00:00+01:00"
draft = false
+++

This post contains mostly unfiltered thoughts - take them with a grain of salt :)

I'm almost finished with the masters degree I've been working on for almost the last 3 years. As my brother points out, that's enough time to finish a PhD, and my undergrad degree was already an (integrated) masters, so once I've graduated I'll be...in exactly the same place I was 3 years ago, academic progression-wise. So was it worth it?

To answer that I'd have to explain why I took the masters in the first place. I had a few different options available to me:

- Should I do a part-time masters or quit my job and do this full-time?
- Should I skip the masters stage and go straight for a PhD?
- Should I do a masters in Computer Science or Statistics?
- Should I find an in-person masters or do a remote one?

The most important factor was simply that I wanted to bootstrap my way to being competent at AI engineering. I planned to eventually join the growing industry of AI Alignment research labs, the entry requirements for which seemed to be either "have an ML PhD" or "be a really competent engineer". Since I wasn't really sure which path I was more suited for, I decided to hedge and pick something which seemed to leave both doors open.

I decided to go with a part-time, remote CS masters. In particular, I chose the OMSCS program at Georgia Tech. It has a few benefits: firstly, for a remote masters degree at a prestigious university, it's very cheap (around €8,000). Secondly, the speed at which the masters proceeds is very flexible - you can complete the degree in less than 2 years if you're really keen, or you can spread it out over 5 years. Thirdly, it offers a specialisation in Machine Learning with a decent breadth and depth of courses.

I chose this masters partially for reasons unrelated to the program itself, but about my particular circumstances at the time, which I'll briefly outline here. Firstly, I was ineligible to apply for masters degrees where I live (Berlin), due to either language or bachelors restrictions (my undergrad degree was in Mathematics and Philosophy, and I was missing some apparently non-negotiable prerequisites). For personal reasons, I was unwilling to move city or country.

Something I regret is not trying to apply for PhD programs. I think I would have had a decent chance of getting in, but I "self-filtered" and decided I should have a relevant masters first.

Finally, a very powerful reason for me to do a masters was as a commitment device. It's true that I could have learned the same thing faster and more cheaply by buying (or otherwise acquiring) textbooks, using MOOCs, joining online study groups, etc. But the fact I am paying ~€800 per semester, and the fact that all work I do accumulates towards my GPA, is a really strong factor keeping me going even when my motivation is running low.

OK, enough about me, let's talk about OMSCS. The program is very flexible: of the 10 course choices, you must choose a few "core" courses for one of 4 specialisations (Computational Perception & Robotics, Computing Systems, Interactive Intelligence, and Machine Learning), and the rest you're free to select from the ~59 options.

I'll start with the good stuff: on average, the quality of the courses is at least as high as any decent MOOC I've taken. Courses are multi-modal, with videos, reading, interactive quizzes, coding and exam components. Serious care has been taken to ensure that everything works smoothly online and across time zones.

Some courses I've learned a huge amount from - in particular, Machine Learning, Deep Learning and Reinforcement Learning. Those courses are challenging but cover a lot of ground. I can tangibly feel the success of my learning by noticing how much easier I find technical research papers to understand now.

There's also a lot that's not great. Firstly, the masters suffers from a lack of research opportunities. To be fair, this is slowly changing, but for example to take a semester and write a research thesis with an advisor requires you to do very well in specific courses, impress specific professors and get substantially lucky.

Personally I found much of this masters to be quite lonely. This was exacerbated by the pandemic: by day, I would work at my desk, and by night I would study alone at my desk. It didn't help that the workload was often very intense, meaning that what remained of my social life got squeezed by day job deadlines and masters assignments.

Some courses are, frankly, not at all worth it. I'm going to specifically call out Knowledge-Based AI. I found it incredibly frustrating and irrelevant. In my opinion, not only is it poorly taught and the material outdated, but the content gap between the lectures and the assignments is huge, making the course feel bewildering and unnecessarily difficult. Avoid!

Finally, in case you're considering taking the OMSCS yourself here's some free (unsolicited) advice from me:

1. Popular courses fill up fast, and the further into the masters you are the higher priority you have. I twice failed to register for the courses I wanted, and settling for a second-choice course killed my motivation. So my advice is to *thoroughly* prepare for course registration. Make a plan on paper, with backups; set alarms; waitlist multiple courses. Don't settle!

- Be active socially, as much as possible - try to meet people on your courses that live close to you in real life, participate in slack discussions and the forums, etc. It's harder work than being social in-person at a university campus, but it's really important for motivation.
- Keep an eye on options available to you that are more interesting than just another taught course. Some examples are:
  - Seminar modules
  - Vertically Integrated Projects, which are interdisciplinary courses available to students
  - The fabled thesis option

I hope this was helpful and/or illuminating! I'll write an update blog post when I finally finish and graduate (in person if possible!).
